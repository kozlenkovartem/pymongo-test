import unittest

from src.app import create_app


class MyTestCase(unittest.TestCase):

    def test_update_conf(self):
        app = create_app({"TESTING": True, 'ENV': 'test'})
        # self.assertIsNotNone(app.testing)

        client = app.test_client()
        self.assertIsNotNone(client)

        # y= client.get('/')

        option = {"username": "a", "password": "a"}
        x = client.post('/new', data=option)
        self.assertIsNotNone(x)


if __name__ == '__main__':
    unittest.main()
