import logging
import os

from flask import Flask, redirect, url_for, request, render_template, app
from pymongo import MongoClient
from flask_pymongo import PyMongo

logger = logging.getLogger("test")
import pymongo


# client = MongoClient("mongodb://mongodb:27017/admin")
# db = client.tododb


def create_app(test_conf=None):
    app = Flask(__name__)
    app.config['MONGO_DBNAME'] = 'tododb'
    app.config['MONGO_URI'] = "mongodb://172.26.0.2:27017/tododb"
    mongo = PyMongo(app)

    if test_conf is not None:
        app.config.update(test_conf)

    @app.route('/')
    def todo():

        try:
            _items = mongo.db.tododb
            items = [item for item in _items]
        except Exception as e:
            logger.error("ConnectionFailure seen: " + str(e))

        return items

    @app.route('/new', methods=['POST'])
    def new():
        if request.method == "POST":
            username = request.form["username"]
            password = request.form["password"]

        item_doc = {
            'name': username,
            'description': password
        }

        return  mongo.db.tododb.insert_one(item_doc).inserted_id

    return app

if __name__ == "__main__":
    create_app(test_conf=None)
